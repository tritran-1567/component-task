<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
<link href="/assets/css/aos.css" rel="stylesheet">
<link href="/assets/css/common.css" rel="stylesheet">
<link href="/assets/css/index.css" rel="stylesheet">
</head>
<body class="page-<?php echo $pageid; ?>">

