<?php /*========================================
table
================================================*/ ?>
<div class="c-dev-title1">table</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table1</div>
<div class="l-container">
	<div class="c-table1">
		<table>
			<tbody>
				<tr>
					<th>社名</th>
					<td>株式会社総和建設</td>
				</tr>
				<tr>
					<th>代表</th>
					<td>依田 弘治</td>
				</tr>
				<tr>
					<th>許可番号</th>
					<td>神奈川県知事　許可（般-○○）第○○○○</td>
				</tr>
				<tr>
					<th>本社・資材センター</th>
					<td>〒243-0211　神奈川県厚木市三田3282-2<br/>TEL：046-280-6667<br/>FAX：046-241-8123</td>
				</tr>
				<tr>
					<th>設立</th>
					<td>2006年6月1日</td>
				</tr>
				<tr>
					<th>資本</th>
					<td>500万円</td>
				</tr>
				<tr>
					<th>従業員数</th>
					<td>20名</td>
				</tr>
				<tr>
					<th>取引銀行</th>
					<td><span>○○銀行△△支店</span><span>○○銀行△△支店 </span></td>
				</tr>
				<tr>
					<th>事業内容</th>
					<td>型枠工事一式</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

 















