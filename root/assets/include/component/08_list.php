<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="l-container">
    <div class="c-list1">
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
        <div class="c-list1__thumb">
            <div class="c-list1__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list1__ttl">
                <p>スタッフ名が入ります</p>
            </div>
            <a href="" class="c-list1__link"></a>
        </div>
    </div>
</div>



<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2</div>
<div class="l-container">
    <div class="c-list2">

<?php for ($i=0;$i<8;$i++){ ?>
        <div class="c-list2__thumb">
            <a href="">
                <div class="c-list2__wrap">
                    <div class="c-list2__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
                    <div class="c-list2__ttl">
                        <p>スタッフ名が入ります</p>
                    </div>
                </div>
            </a>
        </div>
<?php } ?>

    </div>
</div>




<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3</div>
<div class="l-container">
    <div class="c-list3">

<?php for ($i=0;$i<8;$i++){ ?>
        <div class="c-list3__thumb is-link" data-href="https://google.com/" data-target="">
            <div class="c-list3__img"><img src="http://placehold.jp/480x336.png" alt=""></div>
            <div class="c-list3__ttl">
                <p>スタッフ名が入ります</p>
            </div>
        </div>
<?php } ?>

    </div>
</div>

<script>
    
$(function(){
     $(".is-link").click(function(e){
         e.preventDefault();
         if($(this).data("target")=="_blank"){
             window.open($(this).data("href"), '_blank');
         }else{
             window.location=$(this).data("href");
         }
     });
})
    
</script>

