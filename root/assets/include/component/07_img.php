<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgText1</div>
<div class="l-container l-container__ex2">
    <div class="c-imgText1">
        <div class="c-imgText1__img">
            <img src="/assets/img/common/imgTxt1.jpg" alt="">
        </div>
        <div class="c-imgText1__ttl">
            <h3>待合室</h3>
            <p>予約制のため混雑しませんが、快適に過ごしていただくために、<br class="pc-only"/>テレビや雑誌もご用意しております。</p>
        </div>
    </div>
    <div class="c-imgText1 c-imgText1--reverse">
        <div class="c-imgText1__img">
            <img src="/assets/img/common/imgTxt2.jpg" alt="">
        </div>
        <div class="c-imgText1__ttl">
            <h3>キッズルーム</h3>
            <p>小さなお子様をお連れのお母さまが、安心して治療を<br class="pc-only"/>受けられるようにキッズルームもご用意しております。</p>
        </div>
    </div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgText2</div>
<div class="l-container l-container__ex3">
    <div class="c-imgText2">
        <div class="c-imgText2__ttl">
            <h3>精密検査</h3>
            <time><img src="/assets/img/common/clock.jpg" alt=""> 約30分から45分</time>
            <p>治療計画を立案するために、口腔内審査、レントゲン写真（必要であれば歯科用CTも追加致します）、歯型、噛み合わせの検査を行います。</p>
        </div>
        <div class="c-imgText2__img">
            <img src="/assets/img/common/imgTxt3.jpg" class="pc-only" alt="">
            <img src="/assets/img/common/imgTxt3-sp.jpg" class="sp-only" alt="">
        </div>
    </div>
</div>